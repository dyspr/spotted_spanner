var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  path: [128, 128, 128]
}

var boardSize
var size = 17
var groupSet = []
var mazeArray = create2DArray(size, size, 0, false)
for (var i = 0; i < mazeArray.length; i++) {
  for (var j = 0; j < mazeArray[i].length; j++) {
    if (mazeArray[i][j] > -1) {
      groupSet.push([mazeArray[i][j],
        [i, j]
      ])
    }
  }
}
var pickedSet
var neighbours
var pickedNeighbour
var cellsToMerge
var pathHorizontal = create2DArray(size - 1, size, 0, true)
var pathVertical = create2DArray(size, size - 1, 0, true)

var frames = 0
var state = false

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < mazeArray.length; i++) {
    for (var j = 0; j < mazeArray[i].length; j++) {
      fill(64)
      rect(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size) * 0.5, (44 / 768) * boardSize * (17 / size) * 0.5)
    }
  }
  for (var i = 0; i < pathHorizontal.length; i++) {
    for (var j = 0; j < pathHorizontal[i].length; j++) {
      if (pathHorizontal[i][j] === 1) {
        fill(128 + 128 * (mouseX / windowWidth))
      } else {
        noFill()
      }
      rect(windowWidth * 0.5 + (i - Math.floor(size * 0.5) + 0.5) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size) * 0.5, (44 / 768) * boardSize * (17 / size) * 0.5)
    }
  }
  for (var i = 0; i < pathVertical.length; i++) {
    for (var j = 0; j < pathVertical[i].length; j++) {
      if (pathVertical[i][j] === 1) {
        fill(128 + 128 * (mouseY / windowHeight))
      } else {
        noFill()
      }
      rect(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5) + 0.5) * (42 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size) * 0.5, (44 / 768) * boardSize * (17 / size) * 0.5)
    }
  }

  frames += deltaTime * 0.025
  if (frames > 1) {
    frames = 0

    if (groupSet.length !== 1) {
      pickedSet = pickRandomSet(groupSet)
      neighbours = getNeighbours(pickedSet)
      pickedNeighbour = pickRandomSet(neighbours)

      cellsToMerge = getAllCellsToBeMerged(pickedNeighbour, groupSet)
      for (var i = 0; i < cellsToMerge.length; i++) {
        pickedSet.push(cellsToMerge[i])
        mazeArray[cellsToMerge[i][0]][cellsToMerge[i][1]] = pickedSet[0]
      }
      for (var i = 0; i < groupSet.length; i++) {
        if (groupSet[i][0] === pickedNeighbour[0]) {
          groupSet.splice(i, 1)
        }
      }
      // horizontal paths
      if (mazeArray[pickedNeighbour[1][0] - 1] !== undefined) {
        if (mazeArray[pickedNeighbour[1][0] - 1][pickedNeighbour[1][1]] === pickedSet[0]) {
          pathHorizontal[pickedNeighbour[1][0] - 1][pickedNeighbour[1][1]] = 1
        }
      }
      if (mazeArray[pickedNeighbour[1][0] + 1] !== undefined) {
        if (mazeArray[pickedNeighbour[1][0] + 1][pickedNeighbour[1][1]] === pickedSet[0]) {
          pathHorizontal[pickedNeighbour[1][0]][pickedNeighbour[1][1]] = 1
        }
      }
      // vertical paths
      if (mazeArray[pickedNeighbour[1][0]][pickedNeighbour[1][1] - 1] !== undefined) {
        if (mazeArray[pickedNeighbour[1][0]][pickedNeighbour[1][1] - 1] === pickedSet[0]) {
          pathVertical[pickedNeighbour[1][0]][pickedNeighbour[1][1] - 1] = 1
        }
      }
      if (mazeArray[pickedNeighbour[1][0]][pickedNeighbour[1][1] + 1] !== undefined) {
        if (mazeArray[pickedNeighbour[1][0]][pickedNeighbour[1][1] + 1] === pickedSet[0]) {
          pathVertical[pickedNeighbour[1][0]][pickedNeighbour[1][1]] = 1
        }
      }
    } else {
      if (state === true) {
        mazeArray = create2DArray(size, size, 0, false)
        pathHorizontal = create2DArray(size - 1, size, 0, true)
        pathVertical = create2DArray(size, size - 1, 0, true)
        groupSet = []
        for (var i = 0; i < mazeArray.length; i++) {
          for (var j = 0; j < mazeArray[i].length; j++) {
            if (mazeArray[i][j] > -1) {
              groupSet.push([mazeArray[i][j],
                [i, j]
              ])
            }
          }
        }
      }
    }
  }
}

function mousePressed() {
  if (state === false) {
    state = true
    setTimeout(function() {
      state = false
    }, 1)
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function getPaths(set) {
  var horizontal = create2DArray(size - 1, size, 0, true)
  var vertical = create2DArray(size, size - 1, 0, true)
  for (var i = 0; i < set.length; i++) {

  }
  return [horizontal, vertical]
}

function pickRandomSet(set) {
  var randomPick = set[Math.floor(Math.random() * set.length)]
  return randomPick
}

function getNeighbours(set) {
  var neighbours = []
  for (var i = 1; i < set.length; i++) {
    var posX = set[i][0]
    var posY = set[i][1]
    if (posX > 0) {
      if (isArrayInArray(set, [posX - 1, posY]) === false) {
        neighbours.push([mazeArray[posX - 1][posY],
          [posX - 1, posY]
        ])
      }
    }
    if (posX < size - 1) {
      if (isArrayInArray(set, [posX + 1, posY]) === false) {
        neighbours.push([mazeArray[posX + 1][posY],
          [posX + 1, posY]
        ])
      }
    }
    if (posY > 0) {
      if (isArrayInArray(set, [posX, posY - 1]) === false) {
        neighbours.push([mazeArray[posX][posY - 1],
          [posX, posY - 1]
        ])
      }
    }
    if (posY < size - 1) {
      if (isArrayInArray(set, [posX, posY + 1]) === false) {
        neighbours.push([mazeArray[posX][posY + 1],
          [posX, posY + 1]
        ])
      }
    }
  }
  neighbours = multiDimensionalUnique(neighbours)
  return neighbours
}

function getAllCellsToBeMerged(pick, set) {
  var cells = []
  for (var i = 0; i < set.length; i++) {
    if (set[i][0] === pick[0]) {
      for (var j = 1; j < set[i].length; j++) {
        cells.push(set[i][j])
      }
    }
  }
  return cells
}

function multiDimensionalUnique(arr) {
  var uniques = []
  var itemsFound = {}
  for (var i = 0, l = arr.length; i < l; i++) {
    var stringified = JSON.stringify(arr[i])
    if (itemsFound[stringified]) {
      continue
    }
    uniques.push(arr[i])
    itemsFound[stringified] = true
  }
  return uniques
}

function isArrayInArray(arr, item) {
  var item_as_string = JSON.stringify(item)
  var contains = arr.some(function(ele) {
    return JSON.stringify(ele) === item_as_string
  })
  return contains
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
